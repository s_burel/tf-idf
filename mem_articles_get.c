#include "mem_articles.h"

int mem_articles_nba(mem_article * m)
{
	if (m != NULL)
		return m->nba;
	return -1;
}

int mem_articles_next_id(mem_article * m)
{
	if (m != NULL)
		return m->next_id;
	return -1;
}

article * mem_articles_available(mem_article * m)
{
	if (m != NULL)
		return m->available;
	return NULL;
}

article * mem_articles_memory_array(mem_article * m)
{
	if (m != NULL)
		return m->memory;
	return NULL;
}

article * mem_articles_lru(mem_article * m)
{
	if (m != NULL)
		return m->lru;
	return NULL;
}
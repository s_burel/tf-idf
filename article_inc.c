#include "article.h"

int article_inc_number_of_words(article * art)
{
	if (art != NULL)
		return ++art->nbw;
	return -1;
}

int article_inc_number_of_different_words(article * art)
{
	if (art != NULL)
		return ++art->nbdw;
	return -1;
}
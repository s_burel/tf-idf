#ifndef __MEM_WORD_H__
#define __MEM_WORD_H__ 0

#include "word.h"
#include "list.h"
#include "offset.h"
#include "macro.h"

typedef struct struct_mem_word mem_word;

struct struct_mem_word
{
	int nbw; 			/* Number of word in memory */
	offset ** memory; 	/* Memory */
	offset * lru; 		/* Last recently used cache */
	offset * muw; 		/* Most used words cache */
	int nbo; 			/* Number of offsets */
	word ** map; 		/* Array of struct word loaded in memory */
};

/* Getter */
	int mem_word_nbw(mem_word * m);
	offset ** mem_word_memory(mem_word * m);
	int mem_word_nbo(mem_word * m);
	offset * mem_word_lru(mem_word * m);
	offset * mem_word_muw(mem_word * m);
	
/* Setters */
	int mem_word_set_nbw(mem_word * m, int value);
	offset ** mem_word_set_memory(mem_word * m, offset ** value);
	int mem_word_set_nbo(mem_word * m, int value);
	offset * mem_word_set_lru(mem_word * m, offset * value);
	offset * mem_word_set_muw(mem_word * m, offset * value);

/* Array */
	offset * mem_word_n_offset(mem_word * m, int index);
	offset * mem_word_set_n_offset(mem_word * m, int index, offset * o);
	list * mem_word_n_list(mem_word * m, int index);

/* Increment */
	int mem_word_inc_nbw(mem_word * m);
	int mem_word_inc_nbo(mem_word * m);

/* Ram */
	void mem_word_build(mem_word * m);
	/* Realloc new offset and increment number of offset */
	offset ** mem_word_new_offset(mem_word * m);
	void mem_word_free(mem_word * m);
	offset * mem_word_build_cache(mem_word * m, int size);

/* Free */
	void mem_word_free_words(mem_word * m);
	void mem_word_free(mem_word * m);
	void mem_word_free_cache(offset * cache, int size);

/* Lru Handling */
	word * mem_word_update_in_lru(mem_word * m, word * w);
	word * mem_word_search_in_lru(mem_word * m, char * sought);

/* Muw Handling */
	/**
	 * Return NULL if new word must not be inserted
	 * Else, return list where to insert word
	 */
	list * mem_word_mwu_where_to_insert(offset * muw, int new_nba);
	/* Return list containing word in muw, or NULL if word is not 
	 * in muw cache */
	list * mem_word_search_word_in_muw(mem_word * m, word * w);
	word * mem_word_update_in_muw(mem_word * m, word * w);
	word * mem_word_search_in_muw(mem_word * m, char * sought);

/* Search */
	int mem_word_search_offset_index(mem_word * m, char * sought);
	/* Seach for a word in memory, return the offset that (should)
	 * contain the word */
	offset * mem_word_search_offset(mem_word * m, char * sought);
	/**
	 * Return the list before which you must add the searched word
	 * Or the searched word if its found
	 */
	list * mem_word_search_list(list * l, char * sought);
	/* Return the searched word of NULL if word is not found */
	word * mem_word_search_word(mem_word * m, char * sought);

/* Add */
	/* Insert at good place of an offset the given list */
	void mem_word_insert_word_in_list(offset * o, list * new);
	/* Set word in offset an return index of modified offset */
	int mem_word_set_word_in_offset(mem_word * m, word * w);
	/* Add word */
	word * mem_word_add(mem_word * m, char * added);
	/* Add a word if word doesn't exist, and return word struct */
	word * mem_word_add_occurence(mem_word * m, char * added, int aid);
	/* Add first word */
	word * mem_word_first_add(mem_word * m, char * added);

/* Map */
	word ** mem_word_map(mem_word * m);
	word ** mem_word_set_map(mem_word * m, word ** value);
	word * mem_word_map_n_word(mem_word * m, int index);
	word * mem_word_map_set_n_word(mem_word * m, int index, word * value);

/* Map Handling */
	word ** mem_word_map_realloc(mem_word * m);
	int mem_word_map_realloc_is_needed(mem_word * m);
	word ** mem_word_map_realloc_if_needed(mem_word * m);

/* Memory */
	/* Does given offset size so big it needs a realloc ? */
	int mem_word_offset_need_divide(offset * o);
	/* Divide offset at given index if necessary */
	int mem_word_divide_offset_if_needed(mem_word * m, int index);

/* Display */
	void mem_word_display_list(list * l, int fd);
	void mem_word_display(mem_word * m, int fd);

#endif

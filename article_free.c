#include "article.h"

void article_free_name(article * art)
{
	if (article_name(art))
		free(article_name(art));
	article_set_name(art, NULL);
}

void article_free_wid_array(article * art)
{
	if (article_wid_array(art))
		free(article_wid_array(art));
	article_set_wid_array(art, NULL);
}

void article_free_tf_array(article * art)
{
	if (article_tf_array(art))
		free(article_tf_array(art));
	article_set_tf_array(art, NULL);
}

void article_free(article * art)
{
	article_free_tf_array(art);
	article_free_wid_array(art);
	article_free_name(art);
	article_set_number_of_words(art, 0);
}

void article_delete(article ** art)
{
	article_free(*art);
	free(*art);
	*art = NULL;
}
#ifndef __ARTICLE_H__
#define __ARTICLE_H__ 0

#include <string.h>
#include <stdlib.h>

#include "macro.h"

typedef struct struct_article article;

struct struct_article
{
	int aid; /* Article id */
	int nbw; /* Number of words in the article */
	int nbdw; /* Number of different words in the article */
	char * name; /* Content : name of the article */
	float * tf; /* List of term frequency for each word */
	int * wid; /* List of words (words ids) in the article */
};

/* Getters */
	int article_id(article * art);
	int article_number_of_words(article * art);
	char * article_name(article * art);
	int article_number_of_different_words(article * art);

/* Setters */
	int article_set_id(article * art, int i);
	int article_set_number_of_words(article * art, int i);
	char * article_set_name(article * art, char * string);
	int article_set_number_of_different_words(article * art, int i);

/* Wid */
	int * article_wid_array(article * art);
	int * article_set_wid_array(article * art, int * array);
	int article_n_wid(article * art, int x);
	int article_set_n_wid(article * art, int index, int value);

/* Term Frenquency */
	float * article_tf_array(article * art);
	float * article_set_tf_array(article * art, float * array);
	float article_n_tf(article * art, int x);
	int article_set_n_tf(article * art, int index, float value);

/* Ram */
	article * article_new(int id);
	void article_build(article * art);
	/* Realloc wid and tf array */
	int article_realloc_word(article * art, int more);
	char * article_name_copy(article * art, char * string);
	int article_exist(article * art);

/* Free */
	void article_free_name(article * art);
	void article_free_wid_array(article * art);
	void article_free_tf_array(article * art);
	void article_free(article * art);
	void article_delete(article ** art);

/* Handling */
	int article_compare_string(article * art, char * compared);
	float article_add_word_occurence(article * art, int wid);
	int article_realloc_if_needed(article * art);

/* Article Increment */
	int article_inc_number_of_words(article * art);
	int article_inc_number_of_different_words(article * art);

#endif
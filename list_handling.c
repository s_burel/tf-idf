#include "list.h"

void list_insert_after(void * data, list * l)
{
	list * new;
	new = malloc(sizeof(list));
	list_set_prev(new, l);
	list_set_next(new, list_next(l));
	list_set_next(l, new);
	list_set_data(new, data);
}

void list_insert_before(void * data, list * l)
{
	list * new;
	new = malloc(sizeof(list));
	list_set_next(new, l);
	list_set_prev(new, list_prev(l));
	list_set_prev(l, new);
	list_set_data(new, data);
}

void list_move_after(list * moved, list * after_x)
{
	/* Update lists at old location */
	list_set_prev(list_next(moved), list_prev(moved));
	list_set_next(list_prev(moved), list_next(moved));
	/* Update lists at new location */
	list_set_prev(list_next(after_x), moved);
	list_set_prev(moved, after_x);
	list_set_next(moved, list_next(after_x));
	list_set_next(after_x, moved);
}

void list_move_before(list * moved, list * before_x)
{
	/* Update lists at old location */
	list_set_prev(list_next(moved), list_prev(moved));
	list_set_next(list_prev(moved), list_next(moved));
	/* Update lists at new location */
	list_set_next(list_prev(before_x), moved);
	list_set_next(moved, before_x);
	list_set_prev(moved, list_prev(before_x));
	list_set_prev(before_x, moved);
}

#include "mem_articles.h"

void mem_articles_free(mem_article * m)
{
	int i, current_size;
	article * array;
	if (m == NULL) return;

	current_size = mem_articles_size(m);
	for (i = 0; i < current_size; ++i)
	{
		article_free(&(mem_articles_memory_array(m)[i]));
	}

	array = mem_articles_memory_array(m);
	if (array != NULL)
		free(array);
	mem_articles_set_size(m, 0);
}
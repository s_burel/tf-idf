#include "mem_articles.h"

article * mem_articles_search(mem_article * m, char * sought)
{
	int i;
	int count;
	article * focus;

	focus = mem_articles_lru(m);
	if (article_exist(focus))
		if (article_compare_string(focus, sought) == 0) 
			return focus;

	focus = mem_articles_memory_array(m);
	count = mem_articles_size(m);
	for (i = 0; i < count; ++i)
	{
		if (article_exist(&focus[i]))
			 if (article_compare_string(&focus[i], sought) == 0) 
				return &focus[i];
	}
	return NULL;
}

/* Search for available article after current memory cursor */
article * mem_articles_s_av_after_lo(mem_article * m)
{
	article * array;
	int i;
	int j;
	array = mem_articles_memory_array(m);
	j = mem_articles_size(m);
	for (i = mem_articles_nba(m); i < j; ++i)
	{
		if (article_exist(&(array[i])) == 0) {
			mem_articles_set_available(m, &(array[i]));
			return &(array[i]);
		}
	}
	return NULL;	
}

/* Search for available article before current memory cursor */
article * mem_articles_s_av_before_lo(mem_article * m)
{
	article * array;
	int i;
	int j;
	array = mem_articles_memory_array(m);
	j = mem_articles_nba(m);
	for (i = 0; i < j; ++i)
	{
		if (article_exist(&(array[i])) == 0) {
			mem_articles_set_available(m, &(array[i]));
			return &(array[i]);
		}
	}
	return NULL;	
}

article * mem_articles_search_available(mem_article * m)
{
	article * ret;
	ret = mem_articles_s_av_after_lo(m);
	if (ret != NULL) return ret;
	ret = mem_articles_s_av_before_lo(m);
	return ret;
}

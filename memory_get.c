#include "memory.h"

mem_article * memory_articles(memory * m)
{
	if (m == NULL) return NULL;
	return &(m->articles);
}

mem_word * memory_words(memory * m)
{
	if (m == NULL) return NULL;
	return &(m->words);
}

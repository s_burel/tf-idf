#include "offset.h"

list * offset_add_new_head(offset * o, list * value)
{
	list * old_head = offset_head(o);
	list_move_before(value, old_head);
	offset_set_head(o, value);
	offset_inc_size(o);
	return value;
}

list * offset_add_new_tail(offset * o, list * value)
{
	list * old_tail = offset_tail(o);
	list_move_after(value, old_tail);
	offset_set_tail(o, value);
	offset_inc_size(o);
	return value;
}
#include "mem_articles.h"

int mem_articles_realloc_needed(mem_article * m)
{
	if (mem_articles_nba(m) == mem_articles_size(m))
		return 1;
	return 0;
}

/* Add an article, even if article is present in memory */
article * mem_articles_add(mem_article * m, char * name)
{
	article * av;
	/* Realloc if needed */
	if (mem_articles_realloc_needed(m))
		if (mem_articles_realloc(m, MARS) == -1)
			return NULL;

	av = mem_articles_available(m);
	article_set_id(av, mem_articles_inc_next_id(m));
	article_name_copy(av, name);

	mem_articles_inc_nba(m);
	mem_articles_new_available(m);
	return av;
}

/* Return available article or NULL if memory is full */
article * mem_articles_new_available(mem_article * m)
{
	article * available;
	/* Return NULL if memory is full */
	if (mem_articles_realloc_needed(m)) {
		mem_articles_set_available(m, NULL);
		return NULL;
	}
	/* Search for first empty article in the array */
	available = mem_articles_search_available(m);
	mem_articles_set_available(m, available);
	return available;
}

/* Create article if needed, return the article struct */
article * mem_articles_insert(mem_article * m, char * name)
{
	article * art;
	/* Search if article exist in memory */
	art = mem_articles_search(m, name);
	/* If it exist, update lru and return article id */
	if (art != NULL)
		mem_articles_set_lru(m, art);
	/* Else, add an article and return its id */
	else {
		art = mem_articles_add(m, name);
		mem_articles_set_lru(m, art);
	}
	return art;
}

#ifndef __MEMORY_H__
#define __MEMORY_H__ 0

#include "text.h"
#include "mem_articles.h"
#include "mem_word.h"

typedef struct struct_memory memory;

struct struct_memory
{
	mem_article articles;
	mem_word words;
};

/* Getters */
	mem_article * memory_articles(memory * m);
	mem_word * memory_words(memory * m);

/* Ram */
	void memory_build(memory * m);
	void memory_free(memory * m);

/* Handling */
	void memory_add_text(memory * m, text * content);

#endif
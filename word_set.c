#include "word.h"

int word_set_id(word * w, int value)
{
	if (w != NULL)
		return w->wid = value;
	return -1;
}

char * word_set_name(word * w, char * string)
{
	if (w != NULL)
		return w->name = string;
	return NULL;
}

int word_set_nba(word * w, int value)
{
	if (w != NULL)
		return w->nba = value;
	return -1;
}

int * word_set_aid_array(word * w, int * value)
{
	if (w != NULL)
		return w->aid = value;
	return NULL;
}

int word_set_lru(word * w, int value)
{
	if (w != NULL)
		return w->lru = value;
	return -1;
}

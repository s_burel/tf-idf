#include "mem_word.h"

/* Does given offset size so big it needs a realloc ? */
int mem_word_offset_need_divide(offset * o)
{
	int size;
	size = offset_size(o);
	if (size > MWSO)
		return 1;
	return 0;
}

/* Divide offset at given index if necessary */
int mem_word_divide_offset_if_needed(mem_word * m, int index)
{
	int nbo, i;
	offset * o, ** memory;
	list * new_head, * new_tail;
	o = mem_word_n_offset(m, index);
	if (mem_word_offset_need_divide(o)) {
		/* Realloc offset */
		memory = mem_word_new_offset(m);
		if (memory == NULL) return -1;
		nbo = mem_word_nbo(m);
		/* Update memory's offset array */
		for (i = (nbo-1); i > (index+1); --i)
			memory[i] = memory[i-1];
		/* Insert new offset in memory */
		new_tail = memory[index]->tail;
		new_head = offset_divide(memory[index]);
		memory[++index] = malloc(sizeof(offset));
		offset_set_head(memory[index], new_head);
		offset_set_tail(memory[index], new_tail);
		offset_set_size(memory[index], ((MWSO+1) - ((MWSO+1)/2)));
		return 0;
	}
	else return 0;
}

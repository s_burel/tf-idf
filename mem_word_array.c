#include "mem_word.h"

offset * mem_word_n_offset(mem_word * m, int index)
{
	if (m == NULL) return NULL;
	if (mem_word_memory(m) == NULL) return NULL;
	return (mem_word_memory(m)[index]);
}

offset * mem_word_set_n_offset(mem_word * m, int index, offset * value)
{
	offset ** offset;
	if (m == NULL) return NULL;
	offset = mem_word_memory(m);
	if (offset == NULL) return NULL;
	return offset[index] = value;
}

list * mem_word_n_list(mem_word * m, int index)
{
	if (m == NULL) return NULL;
	if (mem_word_memory(m) == NULL) return NULL;
	return (offset_head(mem_word_memory(m)[index]));
}

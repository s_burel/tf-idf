#include "text.h"

char * text_name(text * t)
{
	if (t == NULL) return NULL;
	return t->name;
}

int text_content_size(text * t)
{
	if (t == NULL) return -1;
	return t->content_size;
}

char ** text_content(text * t)
{
	if (t == NULL) return NULL;
	return t->content;
}

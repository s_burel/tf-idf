#include "list.h"

void * list_set_data(list * s, void * value)
{
	if (s != NULL)
		return s->data = value;
	return NULL;
}

list * list_set_prev(list * s, list * value)
{
	if (s != NULL)
		return s->prev = value;
	return NULL;
}

list * list_set_next(list * s, list * value)
{
	if (s != NULL)
		return s->next = value;
	return NULL;
}

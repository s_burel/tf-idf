#include "mem_word.h"

/* Insert at good place of an offset the given list */
void mem_word_insert_word_in_list(offset * o, list * new)
{
	char * added;
	list * l;
	word * w;
	w = (word *)list_data(new);
	added = word_name(w);
	l = offset_head(o);
	w = (word *)list_data(l);
	l = mem_word_search_list(l, added);
	/* If offset first word will be changed by insert */
	if (word_compare_s(w, added) > 0)
		offset_add_new_head(o, new);
	else {
		/* If offset tail will be changed */
		if (l == NULL)
			offset_add_new_tail(o, new);
		/* No change of tail or head */
		else {
			list_move_before(new, l);
			offset_inc_size(o);
		}
	}
}

/* Set word in offset an return index of modified offset */
int mem_word_set_word_in_offset(mem_word * m, word * w)
{
	char * added;
	int i;
	list * new;
	offset * o;
	/* Set word in list */
	added = word_name(w);
	new = list_malloc(w);
	i = mem_word_search_offset_index(m, added);
	o = mem_word_n_offset(m, i);
	mem_word_insert_word_in_list(o, new);
	return i;
}

/* Add word */
word * mem_word_add(mem_word * m, char * added)
{
	int wid, offset_index;
	word * w;
	/* Word Create */
	wid = mem_word_nbw(m);
	w = word_new(wid, added);
	/* Set word in word map */
	mem_word_map_set_n_word(m, wid, w);
	/* Set word in offset */
	offset_index = mem_word_set_word_in_offset(m, w);
	mem_word_divide_offset_if_needed(m, offset_index);
	mem_word_inc_nbw(m);
	/* Set word in lru */
	mem_word_update_in_lru(m, w);
	return w;
}

/* Add a word if word doesn't exist, and return word struct */
word * mem_word_add_occurence(mem_word * m, char * added, int aid)
{
	word * w;
	if (mem_word_nbw(m) == 0) {
		w = mem_word_first_add(m, added);
	} else {
	w = mem_word_search_word(m, added);
	if (w == NULL)
		w = mem_word_add(m, added);
	}
	word_new_occurence(w, aid);
	mem_word_update_in_muw(m, w);
	return w;
}

/* Add first word */
word * mem_word_first_add(mem_word * m, char * added)
{
	list * new;
	offset * off;
	word * w;
	off = (offset *)mem_word_new_offset(m);
	if (off == NULL) return NULL;
	off = ((offset **)off)[0];
	off = malloc(sizeof(offset));
	w = word_new(0, added);
	/* Set word in map */
	mem_word_map_set_n_word(m, 0, w);
	/* Set word in offset */
	new = list_malloc(w);
	offset_set_head(off, new);
	offset_set_tail(off, new);
	offset_set_size(off, 1);
	mem_word_inc_nbw(m);
	mem_word_set_n_offset(m, 0, off);
	return w;
}

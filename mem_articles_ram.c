#include "mem_articles.h"

void mem_articles_build(mem_article * m)
{
	mem_articles_set_nba(m, 0);
	mem_articles_set_size(m, 0);
	mem_articles_set_next_id(m, 1);
	mem_articles_set_available(m, NULL);
	mem_articles_set_memory_array(m, NULL);
	mem_articles_set_lru(m, NULL);
}

int mem_articles_realloc(mem_article * m, int more)
{
	void * ret;
	int current_size;
	int want;
	int i;
	current_size = mem_articles_size(m);
	want = current_size + more;
	want *= sizeof(article);
	ret = realloc(mem_articles_memory_array(m), want);
	if (ret == NULL)
		return -1;
	mem_articles_set_memory_array(m, (article *) ret);
	/* Initialize values */
	want /= sizeof(article);
	for (i = current_size; i < want; ++i)
	{
		article_build(&(mem_articles_memory_array(m)[i]));
	}
	mem_articles_set_size(m, want);
	if (mem_articles_available(m) == NULL)
		mem_articles_set_available(m, (article *) ret);
	return 0;
}

int mem_articles_size(mem_article * m)
{
	if (m == NULL) return -1;
	return m->size_of_memory;
}

int mem_articles_set_size(mem_article * m, int size)
{
	if (m == NULL) return -1;
	return m->size_of_memory = size;
}

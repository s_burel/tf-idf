#include "mem_word.h"

void mem_word_build(mem_word * m)
{
	offset * cache;
	mem_word_set_nbw(m, 0);
	mem_word_set_nbo(m, 0);
	mem_word_set_memory(m, NULL);
	mem_word_set_map(m, NULL);
	cache = mem_word_build_cache(m, MWLRUS);
	mem_word_set_lru(m, cache);
	cache = mem_word_build_cache(m, MWMUS);
	mem_word_set_muw(m, cache);
}

offset * mem_word_build_cache(mem_word * m, int size)
{
	int i;
	list * buffer;
	offset * cache;
	if (m == NULL) return NULL;
	cache = malloc(sizeof(offset));
	buffer = list_malloc(NULL);
	offset_set_head(cache, buffer);
	for (i = 1; i < size; ++i)
	{
		list_insert_after(NULL, buffer);
		buffer = list_next(buffer);
	}
	offset_set_tail(cache, buffer);
	return cache;
}

/* Realloc new offset and increment number of offset */
offset ** mem_word_new_offset(mem_word * m)
{
	int nbo;
	offset ** off;
	nbo = mem_word_nbo(m);
	off = mem_word_memory(m);
	off = realloc(off, sizeof(offset *) * (nbo + 1));
	if (off == NULL) return NULL;
	mem_word_set_memory(m, off);
	mem_word_set_n_offset(m, nbo, NULL);
	mem_word_inc_nbo(m);
	return off;
}

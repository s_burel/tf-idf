#include "mem_word.h"

/*
Avignon
Ici
  <-- super
fais
il
à

*/

int mem_word_search_offset_index(mem_word * m, char * sought)
{
	int begin, end, middle;
	word * w;
	begin = 0;
	end = mem_word_nbo(m);
	middle = (end + begin)/2;
	/* Dichotomic search : */
	/* If begin == middle then begin is the good offset */
	while (begin != middle) {
	/* Compare word with middle : */
		w = (word *) list_data(mem_word_n_list(m, middle));
	/* 		If sought word is smaller than middle, middle is the new end */
		if (word_compare_s(w, sought) > 0)
			end = middle;
	/* 		If word is bigger than middle, middle is the new begin */
		else begin = middle;
	/* Update middle */
		middle = (end + begin)/2;
	}
	return middle;	
}

/* Seach for a word in memory, return the offset that (should)
 * contain the word */
offset * mem_word_search_offset(mem_word * m, char * sought)
{
	return mem_word_n_offset(m, mem_word_search_offset_index(m, sought));
}

/**
 * Return the list before which you must add the searched word
 * or the searched word if its found
 * or NULL if the word must be inserted at end of list
 */
list * mem_word_search_list(list * l, char * sought)
{
	word * w;
	list * buffer = l;
	while(buffer != NULL) {
		w = (word *)list_data(buffer);
		if ((word_compare_s(w, sought)) >= 0)
			return buffer;
		buffer = list_next(buffer);
	}
	return NULL;
}

/* Return the searched word of NULL if word is not found */
word * mem_word_search_word(mem_word * m, char * sought)
{
	offset * off;
	list * l;
	word * w;
	w = mem_word_search_in_lru(m, sought);
	if (w != NULL) return w;
	w = mem_word_search_in_muw(m, sought);
	if (w != NULL) return w;
	off = mem_word_search_offset(m, sought);
	l = offset_head(off);
	l = mem_word_search_list(l, sought);
	if (l == NULL) return NULL;
	if (word_compare_s((word *)list_data(l), sought) == 0) {
		mem_word_update_in_lru(m, (word *)list_data(l));
		return (word *)list_data(l);
	}
	return NULL;
}

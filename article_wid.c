#include "article.h"

int * article_set_wid_array(article * art, int * array)
{
	if (art != NULL)
		return art->wid = array;
	return NULL;
}

int * article_wid_array(article * art)
{
	if (art != NULL)
		return art->wid;
	return NULL;
}

int article_n_wid(article * art, int x)
{
	if (art != NULL)
		if (article_number_of_different_words(art) > x)
			return art->wid[x];
	return 0;
}

int article_set_n_wid(article * art, int index, int value)
{
	if (article_number_of_different_words(art) > index)
		return art->wid[index] = value;
	return -1;
}

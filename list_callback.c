#include "list.h"
#include "context.h"

void * list_execute_on_data(list * l, void * callback(), struct context * c)
{
	if (c->argc == 0)
		return callback(list_data(l));
	return NULL;
}

void * list_global_execute_until_non_null
(list * l, void * callback(), struct context * c)
{
	void * ret;
	list * start;
	list * current;
	list * next;
	
	current = start = l;

	do {
		next = list_next(current);
		ret = list_execute_on_data(current, callback, c);
		if (ret != NULL) return ret;
		current = next;
	} while (current != start && current != NULL);

	return NULL;
}
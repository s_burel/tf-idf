#ifndef __MEM_ARTICLES_H__
#define __MEM_ARTICLES_H__ 0

#include "article.h"

typedef struct struct_mem_article mem_article;

struct struct_mem_article
{
	int nba; 				/* Number of articles in memory */
	article * memory; 		/* Memory */
	article * available; 	/* First available article */
	int next_id; 			/* Next article id */
	article * lru; 			/* Last recently used article */
	int size_of_memory; 	/* Size of memory array */
};

/* Getters */
	int mem_articles_nba(mem_article * m);
	int mem_articles_next_id(mem_article * m);
	article * mem_articles_available(mem_article * m);
	article * mem_articles_memory_array(mem_article * m);
	article * mem_articles_lru(mem_article * m);

/* Setters */
	int mem_articles_set_nba(mem_article * m, int value);
	int mem_articles_set_next_id(mem_article * m, int value);
	article * mem_articles_set_available(mem_article * m, article * av);
	article * mem_articles_set_memory_array(mem_article * m, article * array);
	article * mem_articles_set_lru(mem_article * m, article * value);

/* Ram */
	void mem_articles_build(mem_article * m);
	int mem_articles_realloc(mem_article * m, int more);
	int mem_articles_size(mem_article * m);
	int mem_articles_set_size(mem_article * m, int size);

/* Free */
	void mem_articles_free(mem_article * m);

/* Increment */
	int mem_articles_inc_next_id(mem_article * m);
	int mem_articles_inc_nba(mem_article * m);

/* Search */
	/* Search for available article after current memory cursor */
	article * mem_articles_s_av_after_lo(mem_article * m);
	/* Search for available article before current memory cursor */
	article * mem_articles_s_av_before_lo(mem_article * m);
	article * mem_articles_search_available(mem_article * m);
	article * mem_articles_search(mem_article * m, char * sought);

/* Handling */
	int mem_articles_realloc_needed(mem_article * m);
	/* Add an article, even if article is present in memory */
	article * mem_articles_add(mem_article * m, char * name);
	/* Return available article or NULL if memory is full */
	article * mem_articles_new_available(mem_article * m);
	/* Create article if needed, return the article struct */
	article * mem_articles_insert(mem_article * m, char * name);

#endif

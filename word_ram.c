#include "word.h"
#include "macro.h"

void word_build(word * art)
{
	word_set_nba(art, 0);
	word_set_name(art, NULL);
	word_set_aid_array(art, NULL);
}

int word_realloc_article(word * w, int more)
{
	void * ret;
	int current_size;
	int want;

	current_size = word_nba(w);
	want = current_size + more;

	ret = realloc(word_aid_array(w), sizeof(int) * want);
	if (ret == NULL)
		return -1;
	word_set_aid_array(w, (int *) ret);
	return 0;
}

char * word_name_copy(word * w, char * string)
{
	int size;
	char * w_name;

	w_name = word_name(w);
	if ((w_name != NULL) && (strcmp(w_name, string) == 0)) return w_name;
	
	size = strlen(string);
	if (size > WMSON) return NULL;

	word_free_name(w);
	w_name = malloc(sizeof(char) * size + 1);

	strcpy(w_name, string);
	word_set_name(w, w_name);

	return w_name;
}

word * word_new(int id, char * name)
{
	word * w;
	w = malloc(sizeof(word));
	if (w == NULL) return NULL;
	word_build(w);
	word_set_id(w, id);
	word_name_copy(w, name);
	return w;
}
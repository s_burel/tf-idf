#ifndef __TEXT_H__
#define __TEXT_H__ 0

#include <stdlib.h>
#include <string.h>

typedef struct struct_text text;

struct struct_text
{
	char * name;
	int content_size;
	char ** content;
};

/* Getters */
	char * text_name(text * t);
	int text_content_size(text * t);
	char ** text_content(text * t);

/* Setters */
	char * text_set_name(text * t, char * value);
	int text_set_content_size(text * t, int value);
	int text_inc_content_size(text * t);
	char ** text_set_content(text * t, char ** value);

/* Ram */
	void text_build(text * t);
	void text_free(text * t);
	char * text_malloc_and_copy(char * destination, char * source);
	char * text_copy_name(text * t, char * value);
	char * text_add_content(text * t, char * value);

#endif
#ifndef __WORD_H__
#define __WORD_H__ 0

#define _POSIX_C_SOURCE 200809L

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "macro.h"

typedef struct struct_word word;

struct struct_word
{
	int wid; /* Word id */
	char * name; /* "Content" of the word */
	int nba; /* Number of articles in which the word occurs */
	int * aid; /* List of articles id in which the word occurs */
	int lru; /* Index of last used article in the artice id array */
};

/* Setters */
	int word_set_id(word * w, int value);
	char * word_set_name(word * w, char * string);
	int word_set_nba(word * w, int value);
	int * word_set_aid_array(word * w, int * value);
	int word_set_lru(word * w, int value);

/* Getters */
	int word_id(word * w);
	char * word_name(word * w);
	int word_nba(word * w);
	int * word_aid_array(word * w);
	int word_lru(word * w);

/* Array */
	int word_n_aid(word * w, int index);
	int word_set_n_aid(word * w, int index, int value);

/* Ram */
	void word_build(word * art);
	int word_realloc_article(word * w, int more);
	char * word_name_copy(word * w, char * string);
	word * word_new(int id, char * name);

/* Free */
	void word_free_name(word * w);
	void word_free_aid_array(word * w);
	void word_free(word * w);
	void word_delete(word ** w);

/* Handling */
	int word_compare_s(word * w, char * sought);
	int word_dprintf(word * w, int fd);
	int word_new_aid(word * w, int aid);
	int word_new_occurence(word * w, int aid);
	/* Search given aid and return its index of -1 if not found */
	int word_search_aid(word * w, int aid);

#endif
#include "offset.h"

list * offset_head(offset * o)
{
	if (o == NULL) return NULL;
	return o->head;
}

list * offset_tail(offset * o)
{
	if (o == NULL) return NULL;
	return o->tail;
}

int offset_size(offset * o)
{
	if (o == NULL) return -1;
	return o->size;
}

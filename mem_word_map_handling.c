#include "mem_word.h"

word ** mem_word_map_realloc(mem_word * m)
{
	word ** ret;
	if (m == NULL) return NULL;
	ret = mem_word_map(m);
	ret = realloc(ret, sizeof(word *) * (mem_word_nbw(m) + MWMR));
	mem_word_set_map(m, ret);
	return ret;
}

int mem_word_map_realloc_is_needed(mem_word * m)
{
	if (m == NULL) return -1;
	if ((mem_word_nbw(m) % MWMR) == 0) return 1;
	return 0;
}

word ** mem_word_map_realloc_if_needed(mem_word * m)
{
	if (m == NULL) return NULL;
	if (mem_word_map_realloc_is_needed(m))
		return mem_word_map_realloc(m);
	return mem_word_map(m);
}

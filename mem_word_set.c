#include "mem_word.h"

int mem_word_set_nbw(mem_word * m, int value)
{
	if (m != NULL)
		return m->nbw = value;
	return -1;
}

offset ** mem_word_set_memory(mem_word * m, offset ** value)
{
	if (m != NULL)
		return m->memory = value;
	return NULL;
}

int mem_word_set_nbo(mem_word * m, int value)
{
	if (m != NULL)
		return m->nbo = value;
	return -1;
}

offset * mem_word_set_lru(mem_word * m, offset * value)
{
	if (m != NULL)
		return m->lru = value;
	return NULL;	
}

offset * mem_word_set_muw(mem_word * m, offset * value)
{
	if (m != NULL)
		return m->muw = value;
	return NULL;	
}

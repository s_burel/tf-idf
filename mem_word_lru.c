#include "mem_word.h"

word * mem_word_update_in_lru(mem_word * m, word * w)
{
	offset * lru;
	list * new;
	lru = mem_word_lru(m);
	new = offset_tail(lru);
	/* Set new tail */
	offset_set_tail(lru, list_prev(new));
	list_move_before(new, offset_head(lru));
	/* Set new head */
	offset_set_head(lru, new);
	list_set_data(new, (void *)w);
	return w;
}

word * mem_word_search_in_lru(mem_word * m, char * sought)
{
	int i;
	offset * lru;
	list * target;
	word * w;
	lru = mem_word_lru(m);
	target = offset_tail(lru);
	w = (word *) list_data(target);
	/* Compare first word */
	if (word_compare_s(w, sought) == 0) return w;
	/* Compare other words */
	for (i = 0; i < MWLRUS; ++i) {
		target = list_next(target);
		w = (word *) list_data(target);
		/* Word found */
		if (word_compare_s(w, sought) == 0)
			return mem_word_update_in_lru(m, w);
	}
	/* No words found */
	return NULL;
}

#include "list.h"

void list_build(list * l)
{
	list_set_prev(l, NULL);
	list_set_next(l, NULL);
	list_set_data(l, NULL);
}

list * list_remove(list * l)
{
	list * prev;
	list * next;

	if (l == NULL) return NULL;
	
	prev = list_prev(l);
	next = list_next(l);

	list_set_next(prev, next);
	list_set_prev(next, prev);

	free(list_data(l));
	free(l);

	return next;
}

void list_destroy(list * l)
{
	list * start;
	list * current;
	list * next;
	
	start = l;
	current = start;

	do {
		next = list_remove(current);
		current = next;
	} while (current != start && current != NULL);
}

list * list_malloc(void * data)
{
	list * new = malloc(sizeof(list));
	list_set_prev(new, NULL);
	list_set_next(new, NULL);
	list_set_data(new, data);
	return new;
}
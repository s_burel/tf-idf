#include "mem_word.h"

int mem_word_inc_nbw(mem_word * m)
{
	return mem_word_set_nbw(m, mem_word_nbw(m) + 1);
}

int mem_word_inc_nbo(mem_word * m)
{
	return mem_word_set_nbo(m, mem_word_nbo(m) + 1);
}

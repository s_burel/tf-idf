#include "word.h"

void word_free_name(word * w)
{
	if (word_name(w))
		free(word_name(w));
	word_set_name(w, NULL);
}

void word_free_aid_array(word * w)
{
	if (word_aid_array(w))
		free(word_aid_array(w));
	word_set_aid_array(w, NULL);
}

void word_free(word * w)
{
	word_free_aid_array(w);
	word_free_name(w);
	word_set_nba(w, 0);
}

void word_delete(word ** w)
{
	word_free(*w);
	free(*w);
	*w = NULL;
}
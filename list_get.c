#include "list.h"

void * list_data(list * s)
{
	if (s == NULL) return NULL;
	return s->data;
}

void * list_n_data(list * s, int index)
{
	int i;
	for (i = 0; i < index; ++i)
	{
		if (s == NULL) return NULL;
		s = list_next(s);
	}
	if (s == NULL) return NULL;
	return s->data;
}

list * list_prev(list * s)
{
	if (s == NULL) return NULL;
	return s->prev;
}

list * list_next(list * s)
{
	if (s == NULL) return NULL;
	return s->next;
}

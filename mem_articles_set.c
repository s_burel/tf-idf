#include "mem_articles.h"

int mem_articles_set_nba(mem_article * m, int value)
{
	if (m != NULL)
		return m->nba = value;
	return -1;
}

int mem_articles_set_next_id(mem_article * m, int value)
{
	if (m != NULL)
		return m->next_id = value;
	return -1;
}

article * mem_articles_set_available(mem_article * m, article * av)
{
	if (m != NULL)
		return m->available = av;
	return NULL;
}

article * mem_articles_set_memory_array(mem_article * m, article * array)
{
	if (m != NULL)
		return m->memory = array;
	return NULL;
}

article * mem_articles_set_lru(mem_article * m, article * value)
{
	if (m != NULL)
		return m->lru = value;
	return NULL;
}
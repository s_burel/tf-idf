#ifndef __OFFSET_H__
#define __OFFSET_H__ 0

#include "list.h"
#include "macro.h"

typedef struct struct_offset offset;

struct struct_offset
{
	list * head;
	int size;
	list * tail;
};

/* Getters */
	list * offset_head(offset * o);
	list * offset_tail(offset * o);
	int offset_size(offset * o);

/* Setters */
	list * offset_set_head(offset * o, list * value);
	list * offset_set_tail(offset * o, list * value);
	int offset_set_size(offset * o, int value);
	int offset_inc_size(offset * o);
	/**
	 * Divide an offset in two lists. Update the offset to fit the first
	 * list, and return first element of the second list
	 */
	list * offset_divide(offset * o);

/* Handling */
	list * offset_add_new_head(offset * o, list * value);
	list * offset_add_new_tail(offset * o, list * value);

#endif
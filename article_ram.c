#include "article.h"
#include "macro.h"

void article_build(article * art)
{
	article_set_id(art, 0);
	article_set_number_of_words(art, 0);
	article_set_number_of_different_words(art, 0);
	article_set_name(art, NULL);
	article_set_tf_array(art, NULL);
	article_set_wid_array(art, NULL);
}

/* Realloc wid and tf array */
int article_realloc_word(article * art, int more)
{
	void * ret;
	int current_size;
	int want;

	current_size = article_number_of_different_words(art);
	want = current_size + more;

	ret = realloc(article_tf_array(art), sizeof(float) * want);
	if (ret == NULL)
		return -1;
	article_set_tf_array(art, (float *) ret);
	
	ret = realloc(article_wid_array(art), sizeof(int) * want);
	if (ret == NULL)
		return -2;
	article_set_wid_array(art, (int *) ret);

	return 0;
}

char * article_name_copy(article * art, char * string)
{
	int size;
	char * a_name;

	a_name = article_name(art);
	if ((a_name != NULL) && (strcmp(a_name, string) == 0)) return a_name;
	
	size = strlen(string);
	if (size > AMSON) return NULL;

	article_free_name(art);
	a_name = malloc(sizeof(char) * size + 1);

	strcpy(a_name, string);
	article_set_name(art, a_name);

	return a_name;
}

article * article_new(int id)
{
	article * art;
	art = malloc(sizeof(article));
	if (art == NULL) return NULL;
	article_build(art);
	article_set_id(art, id);
	return art;
}

int article_exist(article * art)
{
	if (art == NULL) return 0;
	return article_id(art);
}

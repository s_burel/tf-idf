#include "article.h"

float * article_set_tf_array(article * art, float * array)
{
	if (art != NULL)
		return art->tf = array;
	return NULL;
}

float * article_tf_array(article * art)
{
	if (art != NULL)
		return art->tf;
	return NULL;
}

float article_n_tf(article * art, int x)
{
	if (art != NULL)
		if (article_number_of_different_words(art) > x)
		return art->tf[x];
	return 0;
}

int article_set_n_tf(article * art, int index, float value)
{
	if (article_number_of_different_words(art) > index)
		return art->tf[index] = value;
	return -1;
}

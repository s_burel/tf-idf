#include "article.h"

int article_id(article * art)
{
	if (art != NULL)
		return art->aid;
	return -1;
}

int article_number_of_words(article * art)
{
	if (art != NULL)
		return art->nbw;
	return -1;
}

int article_number_of_different_words(article * art)
{
	if (art != NULL)
		return art->nbdw;
	return -1;
}

char * article_name(article * art)
{
	if (art != NULL)
		return art->name;
	return NULL;
}

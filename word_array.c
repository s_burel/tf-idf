#include "word.h"

int word_set_n_aid(word * w, int index, int value)
{
	if ((word_nba(w)+WRAA) > index)
		return w->aid[index] = value;
	return -1;
}

int word_n_aid(word * w, int index)
{
	if (word_nba(w) > index)
		return w->aid[index];
	return -1;
}

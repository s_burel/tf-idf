#include "text.h"

char * text_set_name(text * t, char * value)
{
	if (t == NULL) return NULL;
	return t->name = value;
}

int text_set_content_size(text * t, int value)
{
	if (t == NULL) return -1;
	return t->content_size = value;
}

int text_inc_content_size(text * t)
{
	if (t == NULL) return -1;
	return ++(t->content_size);
}

char ** text_set_content(text * t, char ** value)
{
	if (t == NULL) return NULL;
	return t->content = value;
}

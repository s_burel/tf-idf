#include "word.h"

int word_compare_s(word * w, char * sought)
{
	if (w == NULL || word_name(w) == NULL) return -1;
	if (sought == NULL) return 1;
	return strcmp(word_name(w), sought);
}

int word_dprintf(word * w, int fd)
{
	return dprintf(fd, "%s", word_name(w));
}

int word_new_aid(word * w, int aid)
{
	int nba = word_nba(w);
	if ((nba % WRAA) == 0) {
		word_realloc_article(w, WRAA);
	}
	word_set_n_aid(w, nba, aid);
	word_set_nba(w, ++nba);
	return nba-1;
}

int word_new_occurence(word * w, int aid)
{
	int index;
	index = word_search_aid(w, aid);
	if (index != -1) return index;
	return word_new_aid(w, aid);
}

/* Search given aid and return its index of -1 if not found */
int word_search_aid(word * w, int aid)
{
	int nba, buffer;
	for (nba = word_nba(w) -1 ; nba >= 0; --nba)
	{
		buffer = word_n_aid(w, nba);
		if (buffer == aid)
			return nba;
	}
	return nba;
}

CC=gcc
CFLAGS=-std=c99 -g3 -gdwarf -W -Wall -ansi -pedantic -fPIC -Werror
EXEC=start
libs=
obj=memory.o \
	mem_articles.o \
	mem_word.o \
	article.o \
	offset.o \
	word.o \
	list.o \
	text.o

all : $(EXEC)

memory.o : \
	memory_get.o \
	memory_handling.o \
	memory_ram.o
	ar rcs $@ $^

mem_articles.o : \
	mem_articles_get.o \
	mem_articles_set.o \
	mem_articles_ram.o \
	mem_articles_free.o \
	mem_articles_handling.o \
	mem_articles_search.o \
	mem_articles_inc.o
	ar rcs $@ $^

mem_word.o : \
	mem_word_add.o \
	mem_word_array.o \
	mem_word_display.o \
	mem_word_free.o \
	mem_word_get.o  \
	mem_word_inc.o \
	mem_word_lru.o \
	mem_word_map.o \
	mem_word_map_handling.o \
	mem_word_memory.o \
	mem_word_muw.o \
	mem_word_ram.o \
	mem_word_search.o \
	mem_word_set.o
	ar rcs $@ $^

list.o : \
	list_callback.o \
	list_get.o \
	list_handling.o \
	list_ram.o \
	list_set.o
	ar rcs $@ $^

article.o : \
	article_free.o \
	article_get.o \
	article_handling.o \
	article_inc.o \
	article_ram.o \
	article_set.o \
	article_tf.o \
	article_wid.o
	ar rcs $@ $^

word.o : \
	word_set.o \
	word_get.o \
	word_free.o \
	word_ram.o \
	word_array.o \
	word_handling.o
	ar rcs $@ $^

offset.o : \
	offset_handling.o \
	offset_get.o \
	offset_set.o
	ar rcs $@ $^

text.o : \
	text_get.o \
	text_ram.o \
	text_set.o
	ar rcs $@ $^

test : test.c $(obj)
	gcc $(CFLAGS) -L. -Wall -o $(EXEC) $^

start : main.c $(obj)
	gcc $(CFLAGS) -L. -Wall -o $(EXEC) $^

%.o : %.c
	$(CC) $(CFLAGS) -o $@ -c $<

clean :
	rm -f *.so
	rm -f *.o
	rm -f *.a
	rm -f start

gdb :
	gdb $(EXEC)

valgrind :
	valgrind --leak-check=full ./$(EXEC)
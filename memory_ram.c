#include "memory.h"

void memory_build(memory * m)
{
	mem_word_build(memory_words(m));
	mem_articles_build(memory_articles(m));
}

void memory_free(memory * m)
{
	mem_word_free(memory_words(m));
	mem_articles_free(memory_articles(m));
}
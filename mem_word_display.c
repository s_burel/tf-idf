#include "mem_word.h"

void mem_word_display_list(list * l, int fd)
{
	while (l != NULL) {
		word_dprintf((word *)list_data(l), fd);
		dprintf(fd, "\n");
		l = list_next(l);
	}
	dprintf(fd, "\n");
}

void mem_word_display(mem_word * m, int fd)
{
	int nbo;
	int i;
	list * buffer;
	if (m == NULL) return;
	nbo = mem_word_nbo(m);
	if (nbo == 0) return;
	for (i = 0 ; i < nbo; ++i)
	{
		buffer = mem_word_n_list(m, i);
		mem_word_display_list(buffer, fd);
	}
}

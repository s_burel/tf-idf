#include "mem_word.h"

/**
 * Return NULL if new word must not be inserted
 * Else, return list where to insert word
 */
list * mem_word_mwu_where_to_insert(offset * muw, int new_nba)
{
	int cache_word_nba, i;
	list * cmp;
	cmp = offset_tail(muw);
	cache_word_nba = word_nba((word *)list_data(cmp));
	if (new_nba <= cache_word_nba) return NULL;
	for (i = 1; i < MWMUS; ++i) {
		cmp = list_prev(cmp);
		cache_word_nba = word_nba((word *)list_data(cmp));
		if (new_nba <= cache_word_nba)
			return cmp;
	}
	return cmp;
}

/* Return list containing word in muw, or NULL if word is not in muw cache */
list * mem_word_search_word_in_muw(mem_word * m, word * w)
{
	int i;
	offset * muw;
	list * buffer;
	muw = mem_word_muw(m);
	buffer = offset_tail(muw);
	for (i = 0; i < MWMUS; ++i) {
		if (list_data(buffer) == w)
			return buffer;
	}
	return NULL;
}

word * mem_word_update_in_muw(mem_word * m, word * w)
{
	int new_word_nba;
	offset * muw;
	list * new, * position;
	muw = mem_word_muw(m);
	new_word_nba = word_nba(w);
	position = mem_word_mwu_where_to_insert(muw, new_word_nba);
	/* No change to do */
	if (position == NULL)
		return w;
	/* Change to do */
	/* If word exist in cache, then we must update its instance in cache */
	new = mem_word_search_word_in_muw(m, w);
	/* If word does'nt exist in cache, then we must update tail if needed */
	if (new == NULL) {
		new = offset_tail(muw);
		offset_set_tail(muw, list_prev(new));
		list_set_data(new, (void *) w);
	}
	list_move_before(new, position);
	if (position == offset_head(muw))
		offset_set_head(muw, new);
	return w;
}

word * mem_word_search_in_muw(mem_word * m, char * sought)
{
	int i;
	offset * muw;
	list * target;
	word * w;
	muw = mem_word_muw(m);
	target = offset_tail(muw);
	w = (word *) list_data(target);
	/* Compare first word */
	if (word_compare_s(w, sought) == 0) return w;
	/* Compare other words */
	for (i = 0; i < MWMUS; ++i) {
		target = list_next(target);
		w = (word *) list_data(target);
		/* Word found */
		if (word_compare_s(w, sought) == 0)
			return w;
	}
	/* No words found */
	return NULL;
}

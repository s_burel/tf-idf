#include "article.h"

int article_compare_string(article * art, char * compared)
{
	return strcmp(article_name(art), compared);
}

float article_add_word_occurence(article * art, int wid)
{
	int i, nbdw, tf;
	nbdw = article_number_of_different_words(art);
	for (i = 0; i < nbdw; ++i)	{
		/* If word found */
		if(article_n_wid(art, i) == wid) {
			tf = article_n_tf(art, i);
			tf += 1;
			article_set_n_tf(art, i, tf);
			article_inc_number_of_words(art);
			return tf;
		}
	}
	/* Word not found */
	article_realloc_if_needed(art);
	article_inc_number_of_different_words(art);
	article_inc_number_of_words(art);
	article_set_n_tf(art, i, 1.0);
	article_set_n_wid(art, i, wid);
	return 1.0;
}

int article_realloc_if_needed(article * art)
{
	int nbdw;
	nbdw = article_number_of_different_words(art);
	if ((nbdw % ANBR) == 0) {
		return article_realloc_word(art, ANBR);
	}
	return 0;
}

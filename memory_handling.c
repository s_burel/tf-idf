#include "memory.h"

/* Add given article to memory */
void memory_add_text(memory * m, text * text)
{
	article * art;
	word * buffer;
	char ** content;
	int size, i, aid, wid;
	content = text_content(text);
	size = text_content_size(text);
	/* Add Article */
	art = mem_articles_insert(memory_articles(m), text_name(text));
	for (i = 0; i < size; ++i)
	{
		/* Add word */
		aid = article_id(art);
		buffer = mem_word_add_occurence(memory_words(m), content[i], aid);
		wid = word_id(buffer);
		article_add_word_occurence(art, wid);
	}
}

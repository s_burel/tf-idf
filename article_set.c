#include "article.h"

int article_set_id(article * art, int value)
{
	if (art != NULL)
		return art->aid = value;
	return -1;
}

int article_set_number_of_words(article * art, int value)
{
	if (art != NULL)
		return art->nbw = value;
	return -1;
}

char * article_set_name(article * art, char * string)
{
	if (art != NULL)
		return art->name = string;
	return NULL;
}

int article_set_number_of_different_words(article * art, int value)
{
	if (art != NULL)
		return art->nbdw = value;
	return -1;
}

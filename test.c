#include "memory.h"

int main()
{
	article A;
	article * B;
	word * C;
	mem_article D;
	mem_word E;
	memory F;
	text T;
	/* Article test A */
	article_build(&A);
	article_name_copy(&A, "wesh");
	article_add_word_occurence(&A, 12);
	article_add_word_occurence(&A, 12);
	article_add_word_occurence(&A, 2);
	article_free(&A);
	/* Article test B */
	B = article_new(1);
	article_name_copy(B, "wesh");
	article_delete(&B);
	/* Test for word */
	C = word_new(13, "george");
	word_name_copy(C, "francis");
	word_delete(&C);
	/* Test for memory articles */
	mem_articles_build(&D);
	mem_articles_add(&D, "premier article");
	mem_articles_insert(&D, "premier article");
	mem_articles_insert(&D, "wesh");
	mem_articles_realloc(&D, 150);
	mem_articles_free(&D);
	/* Test for memory word */
	mem_word_build(&E);
	mem_word_add_occurence(&E, "coucou", 404);
	mem_word_add_occurence(&E, "coucou", 401);
	mem_word_add_occurence(&E, "ananas", 402);
	mem_word_add_occurence(&E, "louis", 403);
	mem_word_free(&E);
	/* Test for memory */
	text_build(&T);
	text_copy_name(&T, "Premier article");
	text_add_content(&T, "ici");
	text_add_content(&T, "a");
	text_add_content(&T, "avignon");
	text_add_content(&T, "il");
	text_add_content(&T, "fais");
	text_add_content(&T, "super");
	text_add_content(&T, "chaud");
	text_add_content(&T, "mais");
	text_add_content(&T, "je");
	text_add_content(&T, "ne");
	text_add_content(&T, "sais");
	text_add_content(&T, "pas");
	text_add_content(&T, "d");
	text_add_content(&T, "u");
	text_add_content(&T, "c");
	text_add_content(&T, "o");
	text_add_content(&T, "f");
	text_add_content(&T, "b");
	text_add_content(&T, "h");
	text_add_content(&T, "e");
	text_add_content(&T, "i");
	text_add_content(&T, "k");
	text_add_content(&T, "m");
	text_add_content(&T, "r");
	text_add_content(&T, "n");
	text_add_content(&T, "w");
	text_add_content(&T, "g");
	text_add_content(&T, "a");
	text_add_content(&T, "p");
	text_add_content(&T, "s");
	text_add_content(&T, "j");
	text_add_content(&T, "p");
	text_add_content(&T, "t");
	text_add_content(&T, "y");
	text_add_content(&T, "z");
	text_add_content(&T, "v");
	text_add_content(&T, "x");
	text_add_content(&T, "l");
	text_add_content(&T, "q");
	text_add_content(&T, "pourquoi");
	memory_build(&F);
	memory_add_text(&F, &T);
	mem_word_display(&(F.words), 1);
	text_free(&T);
	memory_free(&F);
	return 0;
}

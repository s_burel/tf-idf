#include "text.h"

void text_build(text * t)
{
	text_set_name(t, NULL);
	text_set_content_size(t, 0);
	text_set_content(t, NULL);
}

void text_free(text * t)
{
	int i;
	char ** content;
	content = text_content(t);
	free(text_name(t));
	for (i = text_content_size(t) -1 ; i >= 0; --i) {
		free(content[i]);
	}
	free(content);
}

char * text_malloc_and_copy(char * destination, char * source)
{
	int length;
	length = strlen(source) + 1;
	if (destination != NULL) free(destination);
	destination = malloc(sizeof(char) * length);
	strcpy(destination, source);
	return destination;
}

char * text_copy_name(text * t, char * value)
{
	char * name;
	if (t == NULL) return NULL;
	name = text_name(t);
	name = text_malloc_and_copy(name, value);
	text_set_name(t, name);
	return name;
}

char * text_add_content(text * t, char * value)
{
	int size;
	char * new_word, ** content;
	if (t == NULL) return NULL;
	size = text_content_size(t) + 1;
	content = text_content(t);
	content = realloc(content, sizeof(char *) * size);
	text_set_content(t, content);
	new_word = NULL;
	new_word = text_malloc_and_copy(new_word, value);
	content[size - 1] = new_word;
	text_set_content_size(t, size);
	return content[size - 1];
}

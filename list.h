#ifndef __LIST_H__
#define __LIST_H__ 0

#include <stdlib.h>
#include "context.h"

typedef struct struct_list list;

struct struct_list
{
    list * prev;
    list * next;
    void * data;
};

/* Getters */
void * list_data(list * s);
list * list_prev(list * s);
list * list_next(list * s);
void * list_n_data(list * s, int index);

/* Setters */
void * list_set_data(list * s, void * value);
list * list_set_prev(list * s, list * value);
list * list_set_next(list * s, list * value);

/* Callback */
void * list_execute_on_data(list * l, void * callback(), struct context * c);
void * list_global_execute_until_non_null
(list * l, void * callback(), struct context * c);

/* Handling */
void list_insert_after(void * data, list * l);
void list_insert_before(void * data, list * l);
void list_move_after(list * moved, list * after_x);
void list_move_before(list * moved, list * before_x);

/* Ram */
void list_build(list * l);
list * list_remove(list * l);
void list_destroy(list * l);
list * list_malloc(void * data);

#endif
#include "offset.h"

list * offset_set_head(offset * o, list * value)
{
	if (o == NULL) return NULL;
	return o->head = value;
}

list * offset_set_tail(offset * o, list * value)
{
	if (o == NULL) return NULL;
	return o->tail = value;
}

int offset_set_size(offset * o, int value)
{
	if (o == NULL) return -1;
	return o->size = value;
}

int offset_inc_size(offset * o)
{
	if (o == NULL) return -1;
	return offset_set_size(o, offset_size(o) + 1);
}

/**
 * Divide an offset in two lists. Update the offset to fit the first
 * list, and return first element of the second list
 */
list * offset_divide(offset * o)
{
	int i;
	list * buffer;
	buffer = offset_head(o);
	for (i = offset_size(o)/2; i > 0; --i)
	{
		buffer = list_next(buffer);
	}
	offset_set_tail(o, list_prev(buffer));
	list_set_next(list_prev(buffer), NULL);
	list_set_prev(buffer, NULL);
	offset_set_size(o, offset_size(o)/2);
	return buffer;
}
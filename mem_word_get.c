#include "mem_word.h"

int mem_word_nbw(mem_word * m)
{
	if (m != NULL)
		return m->nbw;
	return -1;
}

offset ** mem_word_memory(mem_word * m)
{
	if (m != NULL)
		return m->memory;
	return NULL;
}

int mem_word_nbo(mem_word * m)
{
	if (m != NULL)
		return m->nbo;
	return -1;	
}

offset * mem_word_lru(mem_word * m)
{
	if (m != NULL)
		return m->lru;
	return NULL;	
}

offset * mem_word_muw(mem_word * m)
{
	if (m != NULL)
		return m->muw;
	return NULL;	
}

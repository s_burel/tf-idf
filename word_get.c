#include "word.h"

int word_id(word * w)
{
	if (w != NULL)
		return w->wid;
	return -1;
}

char * word_name(word * w)
{
	if (w != NULL)
		return w->name;
	return NULL;
}

int word_nba(word * w)
{
	if (w != NULL)
		return w->nba;
	return -1;
}

int * word_aid_array(word * w)
{
	if (w != NULL)
		return w->aid;
	return NULL;
}

int word_lru(word * w)
{
	if (w != NULL)
		return w->lru;
	return -1;
}

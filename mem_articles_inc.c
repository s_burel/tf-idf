#include "mem_articles.h"

int mem_articles_inc_next_id(mem_article * m)
{
	if (m == NULL) return -1;
	return m->next_id++;
}

int mem_articles_inc_nba(mem_article * m)
{
	return mem_articles_set_nba(m, mem_articles_nba(m) + 1);
}

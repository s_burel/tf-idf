#include "mem_word.h"

word ** mem_word_map(mem_word * m)
{
	if (m == NULL) return NULL;
	return m->map;
}

word ** mem_word_set_map(mem_word * m, word ** value)
{
	if (m == NULL) return NULL;
	return m->map = value;
}

word * mem_word_map_n_word(mem_word * m, int index)
{
	word ** map;
	if (m == NULL) return NULL;
	map = mem_word_map(m);
	return map[index];
}

word * mem_word_map_set_n_word(mem_word * m, int index, word * value)
{
	word ** map;
	if (m == NULL) return NULL;
	mem_word_map_realloc_if_needed(m);
	map = mem_word_map(m);
	return map[index] = value;
}

#include "mem_word.h"

void mem_word_free_words(mem_word * m)
{
	int nbo;
	list * buffer;
	if (m == NULL) return;
	nbo = mem_word_nbo(m);
	if (nbo == 0) return;
	for (--nbo ; nbo >= 0; --nbo)
	{
		buffer = mem_word_n_list(m, nbo);
		while (buffer != NULL) {
			word_free((word *)list_data(buffer));
			free(list_data(buffer));
			free(list_prev(buffer));
			if (list_next(buffer) == NULL) {
				free(buffer);
				buffer = NULL;
			} else buffer = list_next(buffer);
		}
		free(mem_word_n_offset(m, nbo));
	}
}


void mem_word_free(mem_word * m)
{
	offset * cache;
	if (m == NULL) return;
	mem_word_free_words(m);
	/* Free mem word */
	free(mem_word_memory(m));
	free(mem_word_map(m));
	/* Free LRU lru */
	cache = mem_word_lru(m);
	mem_word_free_cache(cache, MWLRUS);
	cache = mem_word_muw(m);
	mem_word_free_cache(cache, MWMUS);
}

void mem_word_free_cache(offset * cache, int size)
{
	int i;
	list * buffer;
	buffer = offset_head(cache);
	buffer = list_next(buffer);
	for (i = 1; i < size; ++i)
	{
		free(list_prev(buffer));
		buffer = list_next(buffer);
	}
	buffer = offset_tail(cache);
	free(buffer);
	free(cache);
}
